<?php

$students = [
    'bob_1' => [
        'firstName' => 'Bob',
        'lastName' => 'Robbinson'
    ],
    'john_2' =>[
        'firstName' => 'John',
        'lastName' => 'Robbinson'
    ],
    'ann_3' => [
        'firstName' => 'Ann',
        'lastName' => 'Robbinson'
    ],
];

function fullName($firstName, $lastName = ""){
    return $firstName . ' ' . $lastName;
}

function congratulate($fullName){
    return 'Hi, ' . $fullName . '! Nice to meet you!<br>';
}

foreach($students as $key => $student){
    /*
        $fullStudentName = fullName($student['firstName'], $student['lastName']);
        echo congratulate($fullStudentName);
    */
    echo $key . ' ' . congratulate(fullName($student['firstName'], $student['lastName']));

}