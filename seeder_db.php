<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Entry.php';
$seedEntries = [
    [
        'title' => 'Php is awesome',
        'body' => "Some text about how we like PHP!"
    ],
    [
        'title' => 'Javascript is awesome',
        'body' => "Some text about how we like Javascript"
    ],
    [
        'title' => 'Html is awesome',
        'body' => "Some text about how we like Html"
    ],
    [
        'title' => 'CSS is awesome',
        'body' => "Some text about how we like CSS"
    ],
];

foreach ($seedEntries as $seedEntry) {
    $entry = Entry::create($seedEntry['title'], $seedEntry['body'], $pdo);
}

echo 'Data added successfully';
