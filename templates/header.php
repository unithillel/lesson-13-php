<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="row">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/">Homepage</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/entries/create.php">Create blog entry</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/create_db.php">Create table</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/seeder_db.php">Fill with test data</a>
            </li>
        </ul>
    </div>
</div>