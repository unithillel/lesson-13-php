<?php
class Entry
{
    protected $id;
    protected $title;
    protected $body;
    protected $dateCreated;
    protected $image;
    static public $count = 0;

    public function __construct($id = NULL, $title, $body, $dateCreated = null, $image = null)
    {
        $this->id = $id;
        $this->setTitle($title);
        $this->body = $body;
        $this->dateCreated = $dateCreated;
        $this->image = $image;
        self::$count++;
    }

    static public function find($id, PDO $db)
    {
        $sql = "SELECT * FROM entries WHERE id=:id";
        $responseStatement = $db->prepare($sql);
        $responseStatement->bindValue(':id', $id);
        $responseStatement->execute();
        $data = $responseStatement->fetchAll();
        $entryArray = $data[0];
        $entry = new self(
            $entryArray['id'],
            $entryArray['title'],
            $entryArray['body'],
            $entryArray['date_created'],
            $entryArray['image']
        );
        return $entry;
    }

    static public function all(PDO $db)
    {
        try {
            $sql = "SELECT * FROM entries;";
            $responseStatementObject = $db->query($sql);
            $entriesArray = $responseStatementObject->fetchAll();
            $objects = [];
            foreach ($entriesArray as $entry) {
                $objects[] = new self(
                    $entry['id'],
                    $entry['title'],
                    $entry['body'],
                    $entry['date_created']
                );
            };
        } catch (Exception $e) {
            die('Problem with getting data<br>' . $e->getMessage());
        }
        return $objects;
    }

    static public function destroy($id, PDO $db)
    {
        $sql = "DELETE FROM entries WHERE id=:id";
        try {
            $statement = $db->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
        } catch (Exception $e) {
            die('Error deleting entries.<br>' . $e->getMessage());
        }
    }

    static public function create($title, $body, $image = null, PDO $db)
    {
        $entry = new self(null, $title, $body, null, $image);
        $entry->save($db);
        return $entry;
    }

    public function getTitle()
    {
        return strip_tags($this->title);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBody()
    {
        return strip_tags($this->body);
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setTitle($title)
    {
        $this->title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
    }

    public function save(PDO $db)
    {
        try {
            $sql = "INSERT INTO entries SET
                title = :title,
                body = :body,
                image= :image,
                date_created = CURDATE();
            ";
            $preparedStatement = $db->prepare($sql);
            $preparedStatement->bindValue(':title', $this->title);
            $preparedStatement->bindValue(':body', $this->body);
            $preparedStatement->bindValue(':image', $this->image);
            $preparedStatement->execute();
        } catch (Exception $e) {
            die('Problem with saving entry<br>' . $e->getMessage());
        }
    }

    public function update($db)
    {
        try {
            $sql = "UPDATE entries SET 
                title = :title,
                body = :body
                WHERE id=:id;
            ";
            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':body', $this->body);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        } catch (Exception $e) {
            die('Error updating entry.' . $e->getMessage());
        }
    }
}
