<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';

try{
    $sql = "ALTER TABLE entries ADD COLUMN image VARCHAR(255);";
    $db->exec($sql);
}catch(Exception $e){
    die('Error creating entries table<br>' . $e->getMessage());
}