<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Entry.php';

$entries = Entry::all($db);

$message = NULL;
if (!empty($_GET['message'])) {
    $message = $_GET['message'];
}

?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
<div class="container">
    <h1>Blog entries</h1>
    <?php if ($message) : ?>
        <?php if ($message) : ?>
            <?php switch ($message):
                case 'created': ?>
                    <div class="alert alert-success">
                        Entry created successfully!
                    </div>
                    <?php break; ?>
                <?php
                case 'entry_updated': ?>
                    <div class="alert alert-primary">
                        Entry updated successfully!
                    </div>
                    <?php break; ?>
            <?php endswitch; ?>
        <?php endif ?>
    <?php endif ?>
    <div class="row">

        <?php foreach ($entries as $entry) : ?>
            <div class="col-4 card">
                <div class="card-body">
                    <h5 class="card-title"><?= $entry->getTitle() ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?= $entry->getDateCreated() ?></h6>
                    <a href="/entries/show.php?id=<?= $entry->getId(); ?>" class="btn btn-primary">Read more</a>
                    <br>
                    <a href="/entries/edit.php?id=<?= $entry->getId(); ?>" class="btn btn-warning">Edit</a>
                    <br>
                    <form action="/entries/delete.php?id=<?= $entry->getId(); ?>" method="post"><button class="btn btn-danger">Delete</button></form>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'; ?>