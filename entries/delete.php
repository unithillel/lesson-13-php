<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Entry.php';

$id = (int) $_GET['id'];
Entry::destroy($id, $db);
header('Location:/');

die();
