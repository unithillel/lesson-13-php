<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
<div class="container">
    <h1>Create new entry:</h1>
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <form method="post" action="/entries/store.php" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="title" class="form-label">Entry title:</label>
                    <input name='title' type="text" class="form-control" id="title">
                </div>
                <div class="mb-3">
                    <label for="body" class="form-label">Entry text:</label>
                    <textarea name='body' class="form-control" id="body"></textarea>
                </div>
                <div class="mb-3">
                    <label for="image" class="form-label">Image:</label>
                    <input type="file" name="image" id="image">
                </div>
                <div>
                    <button class="btn btn-primary">Create entry</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'; ?>