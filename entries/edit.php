<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Entry.php';
try {
    $id = (int) $_GET['id'];
    $entry = Entry::find($id, $db);
} catch (Exception $e) {
    die('Error getting entry.<br>' . $e->getMessage());
}
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
<div class="container">
    <h1>Edit entry:</h1>
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <form method="post" action="/entries/update.php">
                <input type="hidden" name="id" value="<?= $id ?>">
                <div class="mb-3">
                    <label for="title" class="form-label">Entry title:</label>
                    <input name='title' type="text" class="form-control" id="title" value="<?= $entry->getTitle() ?>">
                </div>
                <div class="mb-3">
                    <label for="body" class="form-label">Entry text:</label>
                    <textarea name='body' class="form-control" id="body"><?= $entry->getBody() ?></textarea>
                </div>
                <div>
                    <button class="btn btn-primary">Edit entry</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'; ?>