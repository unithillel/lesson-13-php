<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Entry.php';
$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$body = htmlspecialchars($_POST['body'], ENT_QUOTES, 'UTF-8');

$uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/images/';
$fileName = time() . '_' . $_FILES['image']['name'];
$fullPath = $uploadDir . $fileName;
move_uploaded_file($_FILES['image']['tmp_name'], $fullPath);

$entry = Entry::create($title, $body, '/images/' . $fileName, $db);

header('Location:/?message=created');
