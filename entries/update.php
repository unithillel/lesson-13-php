<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Entry.php';

$id = (int) $_POST['id'];
$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$body = htmlspecialchars($_POST['body'], ENT_QUOTES, 'UTF-8');

$entry = new Entry($id, $title, $body);
$entry->update($db);

header('Location:/?message=entry_updated');
