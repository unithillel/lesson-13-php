<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Entry.php';
try {
    $id = (int) $_GET['id'];
    $entry = Entry::find($id, $db);
} catch (Exception $e) {
    die('Error getting entry.<br>' . $e->getMessage());
}

?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
<div class="container">
    <h1><?= $entry->getId() ?> <?= $entry->getTitle() ?></h1>
    <div>
        <img src="<?= $entry->getImage() ?>" alt="<?= $entry->getImage() ?>">
    </div>
    <p><?= $entry->getDateCreated() ?></p>
    <div class="row">
        <p>
            <?= $entry->getBody() ?>
        </p>
    </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'; ?>